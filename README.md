# Craft-Ops
#### Spin up a new project in less than an hour!
Built with Salt, Fabric, Vagrant, and Harp for Amazon Web Services, Bitbucket, and CraftCMS. 


### What Craft-Ops provides:
- a virtualbox development enviornment
- an AWS ec2 and rds instance
- version control through private bitbucket repos
- ssh key generation
- md5 hash plugin downloads

### Basic Skills for Usage:
- terminal
- vagrant
- aws (free tier)
- git/bitbucket
- ssh
- plain text configuration

### Compentencies for Development:
- advanced usage skills
- python
- yaml
- salt
- fabric

# TODO
- more/better documentation
- build plan
- feature backlog
- where to put gulp...?
- preview, staging, production
